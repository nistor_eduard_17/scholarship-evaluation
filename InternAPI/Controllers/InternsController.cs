﻿using InternAPI.Models;
using InternAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class InternsController : ControllerBase
    {
        IInternCollectionService _internCollectionService;

        public InternsController(IInternCollectionService internCollectionService)
        {
            _internCollectionService = internCollectionService ?? throw new ArgumentNullException(nameof(internCollectionService));
        }

        [HttpGet]
        public async Task<IActionResult> GetInterns()
        {
            return Ok(await _internCollectionService.GetAll());
        }

        [HttpGet("internId")]
        public async Task<IActionResult> GetInternsById(Guid internId)
        {
            return Ok(await _internCollectionService.Get(internId));
        }

        [HttpPost]
        public async Task<IActionResult> PostIntern([FromBody] Intern intern)
        {
            if (intern == null)
                return BadRequest("Intern datas are empty");
            await _internCollectionService.Create(intern);
            return Ok(await _internCollectionService.GetAll());
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateIntern(Guid id, [FromBody] Intern intern)
        {
            if (!await _internCollectionService.Update(id, intern))
            {
                return await PostIntern(intern);
            }

            return Ok(await _internCollectionService.GetAll());
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteIntern(Guid id)
        {
            if (await _internCollectionService.Delete(id) == false)
                return NotFound("The intern was not found");

            return Ok(await _internCollectionService.GetAll());
        }

    }
}
