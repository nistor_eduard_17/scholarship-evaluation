import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { InternService } from 'src/app/services/intern.service';
import { HomeComponent } from '../home/home.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  
  validateForm: FormGroup;

  firstName: FormControl;
  lastName: FormControl;
  dateOfBirth: FormControl;
  age: FormControl;

  receivedFirstName:string;
  receivedLastName:string;
  receivedDateOfBirth:string;
  receivedAge:Number;

  constructor(private service: InternService,private dialogRef: MatDialogRef<EditComponent>,
    @Inject(MAT_DIALOG_DATA) data
    ) { 
      this.receivedFirstName=data.firstName;
      this.receivedLastName=data.lastName;
      this.receivedAge=data.age;
      this.receivedDateOfBirth=data.dateOfBirth;
    }

  ngOnInit(): void {

    this.firstName = new FormControl(' ', [Validators.required, Validators.minLength(3)]);
    this.lastName = new FormControl(' ', [Validators.required, , Validators.minLength(3)]);
    this.dateOfBirth = new FormControl(' ', [Validators.required,Validators.pattern('[0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9][0-9]')]);
    this.age = new FormControl(' ', [Validators.required]);

    this.validateForm = new FormGroup({
      'firstName': this.firstName,
      'lastName': this.lastName,
      'dateOfBirth': this.dateOfBirth,
      'age': this.age

    });
    
    this.firstName.setValue(this.receivedFirstName);
    this.lastName.setValue(this.receivedLastName);
    this.dateOfBirth.setValue(this.receivedDateOfBirth);
    this.age.setValue(this.receivedAge);
    
  }

  public updateIntern() {
    HomeComponent.rIntern.firstName =this.firstName.value;
    HomeComponent.rIntern.lastName = this.lastName.value;
    HomeComponent.rIntern.dateOfBirth = this.dateOfBirth.value;
    HomeComponent.rIntern.age = this.age.value;
    this.service.UpdateEditedIntern(HomeComponent.rIntern.id).subscribe();
  }

  save() {
    this.updateIntern();
    this.dialogRef.close(this.validateForm.value);
}

close() {
    this.dialogRef.close();
}

}
