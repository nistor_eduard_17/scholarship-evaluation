import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Intern } from 'src/app/intern';
import { InternService } from 'src/app/services/intern.service';
import { AddInternComponent } from '../add-intern/add-intern.component';
import { EditComponent } from '../edit/edit.component';
import { HomeComponent } from '../home/home.component';

@Component({
  selector: 'app-intern',
  templateUrl: './intern.component.html',
  styleUrls: ['./intern.component.scss']
})
export class InternComponent implements OnInit {
  interns: Intern[]=[];
  currentIntern:Intern;

  constructor(private service:InternService,public dialog:MatDialog, private _router:Router) { }

  ngOnInit(): void {
    this.service.getInterns().subscribe((interns:Intern[])=>{
        this.interns=interns;

    })
    ;
  }

  ngOnChanges():void{
    this.service.getInterns().subscribe((interns:Intern[])=>{
      this.interns=interns;

  })
  ;
   }

RemoveIntern(index:string):void{
   this.service.removeIntern(index).subscribe(()=> {
      this.interns.forEach((element,i) => {
        if(element.id===index)
           this.interns.splice(i,1);
      })
    })
}

openDialog(index:string) {

  HomeComponent.rIntern=this.interns.find(intern=>intern.id===index);

  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.data={
    firstName:HomeComponent.rIntern.firstName,
    lastName:HomeComponent.rIntern.lastName,
    dateOfBirth:HomeComponent.rIntern.dateOfBirth,
    age:HomeComponent.rIntern.age
  }
  this.dialog.open(EditComponent, dialogConfig);
}

openAddDialog(){
  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  this.dialog.open(AddInternComponent, dialogConfig);
  this.dialog.afterAllClosed.subscribe();
}
}
