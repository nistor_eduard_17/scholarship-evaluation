import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Intern } from 'src/app/intern';
import { InternService } from 'src/app/services/intern.service';

@Component({
  selector: 'app-add-intern',
  templateUrl: './add-intern.component.html',
  styleUrls: ['./add-intern.component.scss']
})
export class AddInternComponent implements OnInit {

  validateForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  dateOfBirth: FormControl;
  age: FormControl;

  constructor(private service: InternService){}

  ngOnInit(): void {

    this.firstName = new FormControl(' ', [Validators.required, Validators.minLength(3)]);
    this.lastName = new FormControl(' ', [Validators.required, Validators.minLength(3)]);
    this.dateOfBirth = new FormControl(' ',[Validators.required,Validators.pattern('[0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9][0-9]')]);
    this.age = new FormControl(' ', [Validators.required]);

    this.validateForm=new FormGroup({
      'firstName':this.firstName,
      'lastName':this.lastName,
      'dateOfBirth':this.dateOfBirth,
      'age':this.age

    });

  }


  public Submit(){
    const intern:Intern={
      firstName:this.firstName.value,
      lastName:this.lastName.value,
      dateOfBirth:this.dateOfBirth.value,
      age:this.age.value
    }
    this.service.addIntern(intern).subscribe();
    this.service.getInterns().subscribe();
  }

}
