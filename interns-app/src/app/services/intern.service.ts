import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Intern } from '../intern';
import { HomeComponent } from '../modules/home/home.component';

@Injectable({
  providedIn: 'root'
})
export class InternService {

  readonly baseUrl = "https://localhost:44313/interns";
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };


  constructor(private httpClient: HttpClient) { }

  getInterns(): Observable<Intern[]> {
    return this.httpClient.get<Intern[]>(this.baseUrl, this.httpOptions);

  }

  addIntern(intern: Intern) {
    return this.httpClient.post(this.baseUrl , intern, this.httpOptions);
  }

  removeIntern(index: string): Observable<Intern[]> {
    return this.httpClient.delete<Intern[]>(this.baseUrl+'/{'+index+'}' , this.httpOptions);
  }

  ReceivedInterns(index: string) {
    return this.httpClient.get<Intern[]>(this.baseUrl, this.httpOptions).pipe(map((interns: Intern[]) => {
      return interns.filter((intern) =>
        intern.id === index
      )
    }));
  }

  UpdateEditedIntern(index:string){
      return this.httpClient.put<Intern[]>(this.baseUrl+'/{'+index+'}',HomeComponent.rIntern,this.httpOptions); 
  }

  UpdateIntern(index:string,intern:Intern){
    return this.httpClient.put<Intern[]>(this.baseUrl+'/{'+index+'}',intern,this.httpOptions);
  }
}
